// What we want to do here is accept data from our ecowitt gateway.  It sends a
// POST request with all its aggregated sensor data to several different
// services, as well as allowing you to set your own reciever.  We're leveraging
// that here, building a web server that listens for these post requests, then
// moves the data into structs and allows us to repackage it for further use.
// Our main use case right now is to get this sensor data into HomeAssistant.
// It seems like the easiest way to do this will be to leverage the "sensor"
// component from the "MQTT" subsystem in HomeAssistant.
//
// WeatherStation -> This Web App -> MQTT -> HomeAssistant

package main

import (
	"crypto/tls"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	MQTT "github.com/eclipse/paho.mqtt.golang"
)

// WeatherStation holds the data for the ecowitt GW1000 weather station
type WeatherStation struct {
	PASSKEY          string
	StationType      string
	Freq             string
	Model            string
	DateUTC          string
	Temperature      float64
	Humidity         float64
	PressureRelative float64
	PressureAbsolute float64
}

// TemperatureSensor holds the data for an ecowitt temperature sensor (WH31, WH32)
type TemperatureSensor struct {
	ID          int
	Temperature float64
	Humidity    float64
	Battery     float64
}

// RainSensor holds the data for an ecowitt WH40 rain gauge
type RainSensor struct {
	RainRate    float64
	EventRain   float64
	HourlyRain  float64
	DailyRain   float64
	WeeklyRain  float64
	MonthlyRain float64
	YearlyRain  float64
	TotalRain   float64
	Battery     float64
}

// SoilSensor holds the data for an ecowitt WH51 soil moisture sensor
type SoilSensor struct {
	ID       int
	Moisture float64
	Battery  float64
}

// SensorDevice holds the data that homeassistant expects to configure an MQTT sensor
type SensorDevice struct {
	ConfigTopic       string      `json:"-"`
	DeviceClass       string      `json:"device_class"`
	UniqueID          string      `json:"unique_id"`
	Name              string      `json:"name"`
	StateTopic        string      `json:"state_topic"`
	UnitOfMeasurement string      `json:"unit_of_measurement"`
	ValueTemplate     string      `json:"value_template"`
	State             SensorState `json:"-"`
}

// SensorState holds the data for a homeassistant MQTT sensor state update
type SensorState struct {
	Topic            string  `json:"-"`
	Temperature      float64 `json:"temperature,omitempty"`
	Humidity         float64 `json:"humidity,omitempty"`
	PressureRelative float64 `json:"pressure_relative,omitempty"`
	PressureAbsolute float64 `json:"pressure_absolute,omitempty"`
	RainRate         float64 `json:"rate,omitempty"`
	EventRain        float64 `json:"event,omitempty"`
	HourlyRain       float64 `json:"hourly,omitempty"`
	DailyRain        float64 `json:"daily,omitempty"`
	WeeklyRain       float64 `json:"weekly,omitempty"`
	MonthlyRain      float64 `json:"monthly,omitempty"`
	YearlyRain       float64 `json:"yearly,omitempty"`
	TotalRain        float64 `json:"totalrain,omitempty"`
	SoilMoisture     float64 `json:"soilmoisture,omitempty"`
}

var mqttClient MQTT.Client
var topic *string
var qos *int
var retained *bool
var configured bool

func main() {
	// Setup MQTT connection
	hostname, _ := os.Hostname()
	server := flag.String("server", "tcp://127.0.0.1:1883", "The full URL of the MQTT server to connect to")
	topic = flag.String("topic", hostname, "Topic to publish the messages on")
	qos = flag.Int("qos", 0, "The QoS to send the messages at")
	retained = flag.Bool("retained", false, "Are the messages sent with the retained flag")
	clientid := flag.String("clientid", hostname+strconv.Itoa(time.Now().Second()), "A clientid for the connection")
	username := flag.String("username", "", "A username to authenticate to the MQTT server")
	password := flag.String("password", "", "Password to match username")
	flag.Parse()

	connOpts := MQTT.NewClientOptions().AddBroker(*server).SetClientID(*clientid).SetCleanSession(true)
	if *username != "" {
		connOpts.SetUsername(*username)
		if *password != "" {
			connOpts.SetPassword(*password)
		}
	}
	tlsConfig := &tls.Config{InsecureSkipVerify: true, ClientAuth: tls.NoClientCert}
	connOpts.SetTLSConfig(tlsConfig)

	mqttClient = MQTT.NewClient(connOpts)
	if token := mqttClient.Connect(); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
		return
	}

	// Listen for incoming requests from the Weather Station Module (GW1000)
	http.HandleFunc("/", postHandler)
	log.Fatal(http.ListenAndServe(":3210", nil))
}

// postHandler checks to see what weather stations and sensors are included in
// the POST request.  For each weather station and sensor, 2 MQTT topics are
// created.  One is the configuration topic which contains information
// HomeAssistant needs to display updates for the configured sensor.  The other
// one is for sending state changes for each particular device.  Each gateway
// can support up to 8 sensors, as well as the sensor that's directly attached
// to the gateway (temp + humidity + pressure).  The sensors are enumerated like
// so:
//   tempin     (built in temperature sensor)
//   humidityin (built in humidity sensor)
//   baromrelin (built in relative barometric pressure sensor)
//   baromabsin (built in absolute barometric pressure sensor)
//   temp1f     (temperature sensor on channel 1)
//   temp2f     (temperature sensor on channel 2)
// I only have 2 sensors, but I presume that this pattern continues up to 8
func postHandler(w http.ResponseWriter, req *http.Request) {
	if err := req.ParseForm(); err != nil {
		fmt.Printf("ParseForm() err: %v", err)
		return
	}
	var ws *WeatherStation
	var hassSensors []SensorDevice
	var hassStates []SensorState
	// Check to see if there's a weather station
	if req.PostForm.Get("stationtype") != "" {
		ws = new(WeatherStation)
		ws.PASSKEY = req.PostForm.Get("PASSKEY")
		ws.StationType = req.PostForm.Get("stationtype")
		ws.Model = req.PostForm.Get("model")
		ws.Freq = req.PostForm.Get("freq")
		ws.DateUTC = req.PostForm.Get("dateutc")
		fmt.Println(ws.PASSKEY)
		fmt.Println(ws.StationType)
		fmt.Println(ws.Model)
		fmt.Println(ws.Freq)
		fmt.Println(ws.DateUTC)
		// Check for weather station internal sensors
		if f, err := strconv.ParseFloat(req.PostForm.Get("tempinf"), 32); err == nil {
			ws.Temperature = f
		}
		if h, err := strconv.ParseFloat(req.PostForm.Get("humidityin"), 32); err == nil {
			ws.Humidity = h
		}
		if b, err := strconv.ParseFloat(req.PostForm.Get("baromrelin"), 32); err == nil {
			ws.PressureRelative = b
		}
		if b, err := strconv.ParseFloat(req.PostForm.Get("baromabsin"), 32); err == nil {
			ws.PressureAbsolute = b
		}
		sensor := new(SensorDevice)
		sensor.ConfigTopic = "homeassistant/sensor/temperature/config"
		sensor.DeviceClass = "temperature"
		sensor.Name = ws.Model + " Temperature"
		sensor.UniqueID = ws.Model + "-temperature"
		sensor.StateTopic = "homeassistant/sensor/" + ws.Model + "/state"
		sensor.UnitOfMeasurement = "°F"
		sensor.ValueTemplate = "{{value_json.temperature}}"
		hassSensors = append(hassSensors, *sensor)

		sensor = new(SensorDevice)
		sensor.ConfigTopic = "homeassistant/sensor/humidity/config"
		sensor.DeviceClass = "humidity"
		sensor.Name = ws.Model + " Humidity"
		sensor.UniqueID = ws.Model + "-humidity"
		sensor.StateTopic = "homeassistant/sensor/" + ws.Model + "/state"
		sensor.UnitOfMeasurement = "%"
		sensor.ValueTemplate = "{{value_json.humidity}}"
		hassSensors = append(hassSensors, *sensor)

		sensor = new(SensorDevice)
		sensor.ConfigTopic = "homeassistant/sensor/pressurerelative/config"
		sensor.DeviceClass = "pressure"
		sensor.Name = ws.Model + " Relative Pressure"
		sensor.UniqueID = ws.Model + "-relative pressure"
		sensor.StateTopic = "homeassistant/sensor/" + ws.Model + "/state"
		sensor.UnitOfMeasurement = "inHg"
		sensor.ValueTemplate = "{{value_json.pressure_relative}}"
		hassSensors = append(hassSensors, *sensor)

		sensor = new(SensorDevice)
		sensor.ConfigTopic = "homeassistant/sensor/pressureabsolute/config"
		sensor.DeviceClass = "pressure"
		sensor.Name = ws.Model + " Absolute Pressure"
		sensor.UniqueID = ws.Model + "-absolute pressure"
		sensor.StateTopic = "homeassistant/sensor/" + ws.Model + "/state"
		sensor.UnitOfMeasurement = "inHg"
		sensor.ValueTemplate = "{{value_json.pressure_absolute}}"
		hassSensors = append(hassSensors, *sensor)

		state := new(SensorState)
		state.Topic = sensor.StateTopic
		state.Temperature = ws.Temperature
		state.Humidity = ws.Humidity
		state.PressureRelative = ws.PressureRelative
		state.PressureAbsolute = ws.PressureAbsolute
		hassStates = append(hassStates, *state)
	}

	// Check for any indoor temperature sensors (there can be a maximum of 8)
	var tempSensors []TemperatureSensor
	for i := 1; i < 8; i++ {
		if req.PostForm.Get(fmt.Sprintf("temp%df", i)) != "" {
			ts := new(TemperatureSensor)
			ts.ID = i
			if f, err := strconv.ParseFloat(req.PostForm.Get(fmt.Sprintf("temp%df", i)), 32); err == nil {
				ts.Temperature = f
			}
			if h, err := strconv.ParseFloat(req.PostForm.Get(fmt.Sprintf("humidity%d", i)), 32); err == nil {
				ts.Humidity = h
			}
			if b, err := strconv.ParseFloat(req.PostForm.Get(fmt.Sprintf("batt%d", i)), 32); err == nil {
				ts.Battery = b
			}
			tempSensors = append(tempSensors, *ts)

			// Home Assistant temperature sensor device
			hs := new(SensorDevice)
			hs.ConfigTopic = fmt.Sprintf("homeassistant/sensor/insidetemperature%d/config", i)
			hs.DeviceClass = "temperature"
			hs.Name = fmt.Sprintf("WH31-%d Temperature", i)
			hs.UniqueID = fmt.Sprintf("wh31-%d-temperature", i)
			hs.StateTopic = fmt.Sprintf("homeassistant/sensor/wh31-%d/state", i)
			hs.UnitOfMeasurement = "°F"
			hs.ValueTemplate = "{{ value_json.temperature }}"
			hassSensors = append(hassSensors, *hs)

			// Home Assistant humidity sensor device
			hs = new(SensorDevice)
			hs.ConfigTopic = fmt.Sprintf("homeassistant/sensor/insidehumidity%d/config", i)
			hs.DeviceClass = "humidity"
			hs.Name = fmt.Sprintf("WH31-%d Humidity", i)
			hs.UniqueID = fmt.Sprintf("wh31-%d-humidity", i)
			hs.StateTopic = fmt.Sprintf("homeassistant/sensor/wh31-%d/state", i)
			hs.UnitOfMeasurement = "%"
			hs.ValueTemplate = "{{ value_json.humidity }}"
			hassSensors = append(hassSensors, *hs)

			// Home Assistant sensor state update
			state := new(SensorState)
			state.Topic = hs.StateTopic
			state.Temperature = ts.Temperature
			state.Humidity = ts.Humidity
			hassStates = append(hassStates, *state)
		}
	}

	// Check for the outdoor temperature sensors (there can be one)
	//var outTempSensor TemperatureSensor
	if req.PostForm.Get("tempf") != "" {
		ts := new(TemperatureSensor)
		ts.ID = 0
		if f, err := strconv.ParseFloat(req.PostForm.Get("tempf"), 32); err == nil {
			ts.Temperature = f
		}
		if h, err := strconv.ParseFloat(req.PostForm.Get("humidity"), 32); err == nil {
			ts.Humidity = h
		}
		if b, err := strconv.ParseFloat(req.PostForm.Get("wh32batt"), 32); err == nil {
			ts.Battery = b
		}
		tempSensors = append(tempSensors, *ts)

		// Home Assistant temperature sensor device
		hs := new(SensorDevice)
		hs.ConfigTopic = "homeassistant/sensor/outsidetemperature/config"
		hs.DeviceClass = "temperature"
		hs.Name = "Outside"
		hs.UniqueID = "wh32-temperature"
		hs.StateTopic = "homeassistant/sensor/wh32/state"
		hs.UnitOfMeasurement = "°F"
		hs.ValueTemplate = "{{ value_json.temperature }}"
		hassSensors = append(hassSensors, *hs)

		// Home Assistant humidity sensor device
		hs = new(SensorDevice)
		hs.ConfigTopic = "homeassistant/sensor/outsidehumidity/config"
		hs.DeviceClass = "humidity"
		hs.Name = "Outside"
		hs.UniqueID = "wh32-humidity"
		hs.StateTopic = "homeassistant/sensor/wh32/state"
		hs.UnitOfMeasurement = "%"
		hs.ValueTemplate = "{{ value_json.humidity }}"
		hassSensors = append(hassSensors, *hs)

		// Home Assistant sensor state update
		state := new(SensorState)
		state.Topic = hs.StateTopic
		state.Temperature = ts.Temperature
		state.Humidity = ts.Humidity
		hassStates = append(hassStates, *state)
	}

	// Check for a rain gauge
	if req.PostForm.Get("rainratein") != "" {
		rg := new(RainSensor)
		if r, err := strconv.ParseFloat(req.PostForm.Get("rainratein"), 32); err == nil {
			rg.RainRate = r
		}
		if r, err := strconv.ParseFloat(req.PostForm.Get("eventrainin"), 32); err == nil {
			rg.EventRain = r
		}
		if r, err := strconv.ParseFloat(req.PostForm.Get("hourlyrainin"), 32); err == nil {
			rg.HourlyRain = r
		}
		if r, err := strconv.ParseFloat(req.PostForm.Get("dailyrainin"), 32); err == nil {
			rg.DailyRain = r
		}
		if r, err := strconv.ParseFloat(req.PostForm.Get("weeklyrainin"), 32); err == nil {
			rg.WeeklyRain = r
		}
		if r, err := strconv.ParseFloat(req.PostForm.Get("monthlyrainin"), 32); err == nil {
			rg.MonthlyRain = r
		}
		if r, err := strconv.ParseFloat(req.PostForm.Get("yearlyrainin"), 32); err == nil {
			rg.YearlyRain = r
		}
		if r, err := strconv.ParseFloat(req.PostForm.Get("totalrainin"), 32); err == nil {
			rg.TotalRain = r
		}
		if b, err := strconv.ParseFloat(req.PostForm.Get("wh40batt"), 32); err == nil {
			rg.Battery = b
		}

		// Home Assistant rain sensor device
		hs := new(SensorDevice)
		hs.ConfigTopic = "homeassistant/sensor/rate/config"
		hs.DeviceClass = "rain"
		hs.Name = "WH40 RainGauge"
		hs.UniqueID = "wh40-rain-rate"
		hs.StateTopic = "homeassistant/sensor/wh40/state"
		hs.UnitOfMeasurement = "in"
		hs.ValueTemplate = "{{ value_json.rate }}"
		hassSensors = append(hassSensors, *hs)

		hs = new(SensorDevice)
		hs.ConfigTopic = "homeassistant/sensor/event/config"
		hs.DeviceClass = "rain"
		hs.Name = "WH40 RainGauge"
		hs.UniqueID = "wh40-rain-gauge"
		hs.StateTopic = "homeassistant/sensor/wh40/state"
		hs.UnitOfMeasurement = "in"
		hs.ValueTemplate = "{{ value_json.event }}"
		hassSensors = append(hassSensors, *hs)

		hs = new(SensorDevice)
		hs.ConfigTopic = "homeassistant/sensor/hourly/config"
		hs.DeviceClass = "rain"
		hs.Name = "WH40 RainGauge"
		hs.UniqueID = "wh40-rain-hourly"
		hs.StateTopic = "homeassistant/sensor/wh40/state"
		hs.UnitOfMeasurement = "in"
		hs.ValueTemplate = "{{ value_json.hourly }}"
		hassSensors = append(hassSensors, *hs)

		hs = new(SensorDevice)
		hs.ConfigTopic = "homeassistant/sensor/daily/config"
		hs.DeviceClass = "rain"
		hs.Name = "WH40 RainGauge"
		hs.UniqueID = "wh40-rain-daily"
		hs.StateTopic = "homeassistant/sensor/wh40/state"
		hs.UnitOfMeasurement = "in"
		hs.ValueTemplate = "{{ value_json.daily }}"
		hassSensors = append(hassSensors, *hs)

		hs = new(SensorDevice)
		hs.ConfigTopic = "homeassistant/sensor/weekly/config"
		hs.DeviceClass = "rain"
		hs.Name = "WH40 RainGauge"
		hs.UniqueID = "wh40-rain-weekly"
		hs.StateTopic = "homeassistant/sensor/wh40/state"
		hs.UnitOfMeasurement = "in"
		hs.ValueTemplate = "{{ value_json.weekly }}"
		hassSensors = append(hassSensors, *hs)

		hs = new(SensorDevice)
		hs.ConfigTopic = "homeassistant/sensor/monthly/config"
		hs.DeviceClass = "rain"
		hs.Name = "WH40 RainGauge"
		hs.UniqueID = "wh40-rain-monthly"
		hs.StateTopic = "homeassistant/sensor/wh40/state"
		hs.UnitOfMeasurement = "in"
		hs.ValueTemplate = "{{ value_json.monthly }}"
		hassSensors = append(hassSensors, *hs)

		hs = new(SensorDevice)
		hs.ConfigTopic = "homeassistant/sensor/yearly/config"
		hs.DeviceClass = "rain"
		hs.Name = "WH40 RainGauge"
		hs.UniqueID = "wh40-rain-yearly"
		hs.StateTopic = "homeassistant/sensor/wh40/state"
		hs.UnitOfMeasurement = "in"
		hs.ValueTemplate = "{{ value_json.yearly }}"
		hassSensors = append(hassSensors, *hs)

		hs = new(SensorDevice)
		hs.ConfigTopic = "homeassistant/sensor/totalrain/config"
		hs.DeviceClass = "rain"
		hs.Name = "WH40 RainGauge"
		hs.UniqueID = "wh40-rain-total"
		hs.StateTopic = "homeassistant/sensor/wh40/state"
		hs.UnitOfMeasurement = "in"
		hs.ValueTemplate = "{{ value_json.totalrain }}"
		hassSensors = append(hassSensors, *hs)

		// Home Assistant sensor state update
		state := new(SensorState)
		state.Topic = hs.StateTopic
		state.RainRate = rg.RainRate
		state.EventRain = rg.EventRain
		state.HourlyRain = rg.HourlyRain
		state.DailyRain = rg.DailyRain
		state.WeeklyRain = rg.WeeklyRain
		state.MonthlyRain = rg.MonthlyRain
		state.YearlyRain = rg.YearlyRain
		state.TotalRain = rg.TotalRain
		hassStates = append(hassStates, *state)
	}

	// Check for any Soil Moisture sensors (there can be a maximum of 8)
	var soilSensors []SoilSensor
	for i := 1; i < 8; i++ {
		if req.PostForm.Get(fmt.Sprintf("soilmoisture%d", i)) != "" {
			ss := new(SoilSensor)
			ss.ID = i
			if f, err := strconv.ParseFloat(req.PostForm.Get(fmt.Sprintf("soilmoisture%d", i)), 32); err == nil {
				ss.Moisture = f
			}
			if b, err := strconv.ParseFloat(req.PostForm.Get(fmt.Sprintf("soilbatt%d", i)), 32); err == nil {
				ss.Battery = b
			}
			soilSensors = append(soilSensors, *ss)

			// Home Assistant soil moisture sensor device
			hs := new(SensorDevice)
			hs.ConfigTopic = fmt.Sprintf("homeassistant/sensor/soil%d/config", i)
			hs.DeviceClass = "soil"
			hs.Name = fmt.Sprintf("WH51-%d Soil Sensor", i)
			hs.UniqueID = fmt.Sprintf("wh51-%d-soil", i)
			hs.StateTopic = fmt.Sprintf("homeassistant/sensor/wh51-%d/state", i)
			hs.UnitOfMeasurement = "%"
			hs.ValueTemplate = "{{ value_json.soilmoisture }}"
			hassSensors = append(hassSensors, *hs)

			// Home Assistant sensor state update
			state := new(SensorState)
			state.Topic = hs.StateTopic
			state.SoilMoisture = ss.Moisture
			hassStates = append(hassStates, *state)
		}
	}

	// Configure each sensor in HomeAssistant
	if configured == false {
		for _, sensor := range hassSensors {
			m, err := json.Marshal(sensor)
			if err != nil {
				fmt.Println("error:", err)
			}
			fmt.Println("Configuring sensor: " + sensor.Name)
			fmt.Println("Config Topic: " + sensor.ConfigTopic)
			fmt.Println("Unique ID: " + sensor.UniqueID)
			fmt.Println()
			mqttClient.Publish(sensor.ConfigTopic, byte(*qos), *retained, m)
		}
		configured = true
	}

	// Send state updates to HomeAssistant
	for _, state := range hassStates {
		s, err := json.Marshal(state)
		if err != nil {
			fmt.Println("error:", err)
		}
		mqttClient.Publish(state.Topic, byte(*qos), *retained, s)
	}
}
